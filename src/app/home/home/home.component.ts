import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CreateProductService } from 'src/app/services/create-product/create-product.service';
import { FetchProductsService } from 'src/app/services/fetch-products/fetch-products.service';
import { UploadImageService } from 'src/app/services/upload-image/upload-image.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
let products: any[] = [];
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  converted = false;
  isSuccess = false;
  public textConvertForm!: FormGroup;
  public createProductForm!: FormGroup;
  public imageUploadForm!: FormGroup;
  selectedFile: File | undefined;
  totalCount: number = 10;
  pageSize: number = 1000;
  pageIndex: number = 0;
  pageSizeOptions: number[] = [5, 10, 20];
  displayedColumns = ['id', 'name', 'brand', 'image', 'price', 'description'];

  dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  constructor(
    private formBuilder: FormBuilder,
    private createProd: CreateProductService,
    private imgUpld: UploadImageService,
    private prodService: FetchProductsService
  ) {}

  ngOnInit(): void {
    this.textConvertForm = this.formBuilder.group({
      text: ['', [Validators.required]],
    });

    this.createProductForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      brand: ['', [Validators.required]],
      image: [''],
      price: ['', [Validators.required]],
      description: ['', [Validators.required]],
    });

    this.imageUploadForm = this.formBuilder.group({
      image: [''],
    });
    this.loadProducts();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  onFileChange(event: any) {
    let reader = new FileReader();
    let file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);
    }
  }

  uploadImage() {
    const formData = new FormData();
    formData.append('image', this.imageUploadForm.get('image')?.value);
    this.imgUpld.uploadImage(formData).subscribe((res: any) => {
      console.log(res);
    });
  }
  onSubmit(value: any) {
    if (value) {
      this.converted = true;
    } else {
      this.converted = false;
    }
  }

  createProduct() {
    const formData = new FormData();
    formData.append('image', this.createProductForm.get('image')?.value);
    this.createProd
      .createProduct(this.createProductForm.value)
      .subscribe((res: any) => {
        console.log(res);
        if (res.status === 'Success' && res.data.length > 0) {
          const userData = res.data[0];
          localStorage.setItem('user', JSON.stringify(userData));
          this.isSuccess = true;
        } else {
          //create error
        }
        this.createProductForm.reset();
      });
  }

  onPageChange(event: any) {
    this.pageSize = event.pageSize;
  }

  loadProducts() {
    this.prodService
      .getProducts(this.pageSize, 0) //fetch data by service
      .subscribe((data: any) => {
        products = data.data;
        this.totalCount = data.totalCount; //update totalcount
        this.dataSource.data = products;
      });
  }
}
export interface Element {
  id: any;
  name: any;
  brand: any;
  image: any;
  price: any;
  description: any;
}
const ELEMENT_DATA: Element[] = products;

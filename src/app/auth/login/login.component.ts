import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { LoginService } from 'src/app/services/login-service/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  email: any;
  password: any;

  public loginForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private loginService: LoginService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  onSubmit() {
    const loginData = {
      email: 'pooja@gmail',
      password: 'password',
      deviceId: '125',
    };

    this.loginService.login(loginData).subscribe(
      (response: any) => {
        if (response.status === 'Success' && response.data.length > 0) {
          const userData = response.data[0];
          localStorage.setItem('user', JSON.stringify(userData));
        } else {
          // login error
        }
      },
      (error: any) => {
        //erorr
      }
    );
  }
}

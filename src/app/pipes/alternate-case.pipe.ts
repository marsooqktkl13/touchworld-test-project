import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'alternateCase',
})
export class AlternateCasePipe implements PipeTransform {
  transform(text: string): string {
    let result = '';
    for (let i = 0; i < text.length; i++) {
      result += i % 2 === 0 ? text[i].toUpperCase() : text[i].toLowerCase();
    }
    console.log(result);

    return result;
  }
}

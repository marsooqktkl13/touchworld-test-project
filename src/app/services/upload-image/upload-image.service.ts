import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UploadImageService {
  constructor(private http: HttpClient) {}
  uploadImage(imageData: FormData) {
    console.log(imageData);

    return this.http.post(
      'http://testapp.touchworldtechnology.com:3009/v1/product/uploadImage',
      imageData
    );
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CreateProductService {
  private basicUrl = 'http://testapp.touchworldtechnology.com:3009/v1';

  constructor(private http: HttpClient) {}

  createProduct(data: any) {
    const url = `${this.basicUrl}/product/createProduct`;
    return this.http.post(url, data);
  }
}

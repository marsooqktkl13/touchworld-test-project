import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FetchProductsService {
  private basicUrl =
    'http://testapp.touchworldtechnology.com:3009/v1/product/listAllProduct';

  constructor(private http: HttpClient) {}
  getProducts(limit: number, offset: number) {
    const url = `${this.basicUrl}?limit=${limit}&offset=${offset}`;
    return this.http.get(url);
  }
}

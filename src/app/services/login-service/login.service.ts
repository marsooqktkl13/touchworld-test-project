import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  private loginUrl = 'http://testapp.touchworldtechnology.com:3009/v1/login/';
  constructor(private http: HttpClient) {}

  login(loginData: any) {
    return this.http.post(this.loginUrl, loginData);
  }
}
